package seiot.log;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
/**
 * a factory of a single logger object
 */
public class LoggerFactory {
	private static Logger SINGLETON = null;
	private LoggerFactory() {}

	public static Logger instance() {
		if(SINGLETON == null) {
			SINGLETON = new LoggerImpl();
		}
		return SINGLETON;
	}
/**
 * Test class for a Logger
 *
 */
	private static class DebugLogger implements Logger{
		@Override
		public List<String> show() {
			return Arrays.asList("ciao","pinco","pullo");
		}

		@Override
		public void log(String e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void clear() {
			// TODO Auto-generated method stub

		}
	}
/**
 *  It shows daily logs
 *
 */
	private static class LoggerImpl implements Logger {
		private static final File FILE = new File("log.txt");
		@Override
		public void log(String e) throws IOException {
			if(!FILE.exists()) {
				FILE.createNewFile();
			}
			checkDate();
			Files.write(Paths.get(FILE.getName()), Arrays.asList(e), StandardOpenOption.APPEND);
		}

		@Override
		public List<String> show() throws IOException {
			checkDate();
			return Files.readAllLines(Paths.get(FILE.getName()));
		}
		/*It checks if the file has been created today or not*/
		private void checkDate() throws IOException {
			final Date today = new Date();
			final Date lastModified = new Date(FILE.lastModified());
			if(today.getDay() != lastModified.getDay() 
					|| today.getMonth() != lastModified.getMonth()
					|| today.getYear() != lastModified.getYear()) {
				FILE.delete();
				FILE.createNewFile();	
			}
		}
		@Override
		public void clear() {
			FILE.delete();
		}

	}
}
