package seiot.log;

import java.io.IOException;
import java.util.List;

/**
 * define a generic logger
 */
public interface Logger {
	/**
	 * store a log 
	 * @param e the log to store
	 */
	void log(String e) throws IOException ;
	/**
	 * show all log
	 */
	List<String> show() throws IOException;
	/**
	 * clear the log stored
	 */
	void clear();
}
