package seiot.server.door;
/**
 * think if you create an agent or not
 *
 */
public interface Home {
	/**
	 * get the current temperature in the home
	 * @return
	 */
	Double getTemp();
	/**
	 * get the value of l value
	 * @return
	 */
	int getValue();
}
