package seiot.server.door;

import java.util.Optional;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

import seiot.model.Pair;
import seiot.model.agent.AbstractCommunicationManager;
import seiot.model.core.Event;
import seiot.model.msg.Msg;
import seiot.model.msg.MsgEvent;
import seiot.model.msg.MsgGetValues;
import seiot.model.msg.MsgTemp;
import seiot.model.msg.MsgValue;

class SimpleServerAgent extends ServerAgent {
	private static final int DEFAULT_SIZE = 1;
	private BlockingQueue<Double> temp;
	private BlockingQueue<Integer> value;
	private final AbstractCommunicationManager manager;
	
	SimpleServerAgent(final AbstractCommunicationManager manager) {
		this.manager = manager;
		this.temp = new ArrayBlockingQueue<Double>(DEFAULT_SIZE);
		this.value = new ArrayBlockingQueue<Integer>(DEFAULT_SIZE);
	}
	@Override
	protected void processEvent(Event ev) {
		if(ev instanceof MsgEvent) {
			final MsgEvent event = (MsgEvent)ev;
			final Msg msg = event.getMsg();
			if(msg instanceof MsgTemp) {
				this.temp.offer(((MsgTemp) msg).getTemp());
			} else if(msg instanceof MsgValue) {
				this.value.offer(((MsgValue) msg).getValue());
			}
		}
	}
	
	public Pair<Double,Integer> getValue() throws InterruptedException {
		sendMsgTo(this.manager, new MsgGetValues());
		final Pair<Double,Integer> res = Pair.instance(temp.take(), value.take());
		return res;
	}
}
