package seiot.server.door;

import seiot.model.Pair;
import seiot.model.agent.AbstractCommunicationManager;
import seiot.model.core.ReactiveAgent;
/**
 * an abstract class that use a communication manager to take the value of smart home 
 */
public abstract class ServerAgent extends ReactiveAgent {
	//SIMPLE FACTORY
	public static ServerAgent create(final AbstractCommunicationManager comunication) {
		return new SimpleServerAgent(comunication);
		
	}
	/**
	 * 
	 * @return a pair of Double(temp) an Integer(value)
	 * @throws InterruptedException
	 */
	public abstract Pair<Double,Integer> getValue() throws InterruptedException;	
}
