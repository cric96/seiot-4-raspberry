package seiot.server.door;

import java.util.Random;

public class HomeFactory {
	private HomeFactory(){}
	
	public static Home instance(final ServerAgent com) {
		return new HomeImpl(com);
	}
/**
 * Debug class simulating a Home
 * @return random numbers
 */
	private static Home homeDoor() {
		final Random rand = new Random();
		final int bound = 100;
		return new Home() {
			@Override
			public Double getTemp() {
				return rand.nextDouble();
			}

			@Override
			public int getValue() {
				return rand.nextInt(bound);
			}
			
		};
	}
}
