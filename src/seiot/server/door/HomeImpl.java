package seiot.server.door;

import seiot.model.Pair;

class HomeImpl  implements Home {
	private final static int DEFAULT_VALUE = 0;
	private Double temp;
	private int value;
	public HomeImpl(final ServerAgent agent) {
		Pair<Double, Integer> values;
		try {
			values = agent.getValue();
			this.temp = values.getFirst();
			this.value = values.getSecond();
		} catch (InterruptedException e) {
			this.temp = (double) DEFAULT_VALUE;
			this.value = DEFAULT_VALUE;
		}
	}
	@Override
	public Double getTemp() {
		return this.temp;
	}

	@Override
	public int getValue() {
		return this.value;
	}

}
