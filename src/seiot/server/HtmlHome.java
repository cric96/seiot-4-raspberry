package seiot.server;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import seiot.log.LoggerFactory;
import seiot.server.door.Home;
import seiot.server.door.HomeFactory;
import seiot.server.door.ServerAgent;
/**
 * a generic handler of a http request that displays the value in the home
 */
public class HtmlHome implements HttpHandler{
	private final ServerAgent manager;
	
	public HtmlHome(final ServerAgent manger) {
		this.manager = manger;
	}
	@Override
	public void handle(HttpExchange t) throws IOException {
		List<String> logs = LoggerFactory.instance().show();
		String htmlLogs = "<ul>";
		for(String log : logs) {
			htmlLogs += "<li>" + log + "</li>";
		}
		final Home door = HomeFactory.instance(this.manager);
		String tempValue = "<li> temp : " + door.getTemp() + "</li>";
		String vValue = "<li> value : " + door.getValue() + "</li>";
		htmlLogs += "</ul>";
		String response = "<html>"
						+ 	"<head> <title> Smart door </title> </head>"
						+	"<body> "
						+ 		"<h1> Benvenuto </h1> "
						+ 		"<h2> Log: </h2>"
						+		htmlLogs
						+		"<h2> Valori:</h2>"
						+		"<ul>"
						+		tempValue
						+		vValue
						+		"</ul>"
						+ 	"</body>"
						+ "</html>";
        t.sendResponseHeaders(200, response.length());
        OutputStream os = t.getResponseBody();
        os.write(response.getBytes());
        os.close();
        System.out.println("out..");
	}
}