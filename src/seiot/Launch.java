package seiot;


import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

import seiot.model.agent.AbstractCommunicationManager;
import seiot.model.agent.Blinker;
import seiot.model.agent.CommunicationManager;
import seiot.model.agent.Gateway;
import seiot.model.device.Light;
import seiot.model.device.ObservableTimer;
import seiot.server.HtmlHome;
import seiot.server.door.ServerAgent;

public class Launch {
	private static final int BACKLOG = 0; 
	private static final int PIN_FAIL = 11;
	private static final int PIN_ACCESS = 12;
	private Launch(){}
	public static void main(String[] args) throws Exception {
	   final Light failedAccess = new seiot.model.device.p4j.Led(PIN_FAIL);
	   final Light inside = new seiot.model.device.p4j.Led(PIN_ACCESS);
	   //final Light failedAccess = new seiot.model.device.emu.Led(PIN_FAIL);
	   //final Light inside = new seiot.model.device.emu.Led(PIN_ACCESS);
	   
	   final Blinker failBlinker = new Blinker(failedAccess, new ObservableTimer());
	   final Gateway gateway = new Gateway(failBlinker, inside);
	   final AbstractCommunicationManager comunication = new CommunicationManager("COM4");
	   final ServerAgent door = ServerAgent.create(comunication);
	   comunication.attach(gateway);
	   comunication.attach(door);
	   door.start();
	   gateway.start();
	   comunication.start();
	   failBlinker.start();
	   HttpServer server = HttpServer.create(new InetSocketAddress(80),BACKLOG);
	   System.out.println("start service...");
	   server.createContext("/", new HtmlHome(door));
	   server.setExecutor(null); // creates a default executor
	   server.start();
	   
	}
}
