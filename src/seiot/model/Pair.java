package seiot.model;

public interface Pair<T1, T2> {
	T1 getFirst();
	
	T2 getSecond();
	static <T1,T2> Pair<T1,T2> instance(final T1 t1, final T2 t2) {
		return new Pair<T1, T2>() {
			@Override
			public T1 getFirst() {
				return t1;
			}

			@Override
			public T2 getSecond() {
				return t2;
			}
		};
		
	}
}
