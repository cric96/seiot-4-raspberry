package seiot.model.event;

import seiot.model.core.Event;
/**
 * an event produced to say a time
 */
public class TimerDelta implements Event{
    private final int delta;
    
    public TimerDelta(final int delta) {
        this.delta = delta;
    }
    
    public int getDelta(){
        return this.delta;
    }
}
