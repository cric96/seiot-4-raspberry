package seiot.model.agent;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;

public class CommunicationManager extends AbstractCommunicationManager implements SerialPortEventListener{
	private static final int BOUND_RATE = 9600;
	private final InputStream input;
	private final PrintStream output;
	private String builder;
	public CommunicationManager(final String port) throws Exception {
		CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(port);
		SerialPort serialPort = (SerialPort) portId.open(this.getClass().getName(), 2000);
		serialPort.setSerialPortParams(BOUND_RATE,
				SerialPort.DATABITS_8,
				SerialPort.STOPBITS_1,
				SerialPort.PARITY_NONE);
		input = serialPort.getInputStream();
		output = new PrintStream(serialPort.getOutputStream());
		serialPort.addEventListener(this);
		serialPort.notifyOnDataAvailable(true);
		this.builder = "";
	}
	/*It is called when a message come from the serial connection and it sends the message to all 
	 * of the agents attached*/
	@Override
	public void serialEvent(final SerialPortEvent event) {
		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {
				while (input.available() > 0){
					char inputChar=(char)input.read();
					if(inputChar != '\r' && inputChar != '\n') {
						this.builder+= inputChar;
					}
					if(inputChar == '\n' ) {
						sendTo(builder);
						builder = "";
					}
				}
			} catch (IOException e) {		}
		}
	}
	

/*It defines how to send messages using serial connection*/
	@Override
	protected void out(String string) {
		for(char c :string.toCharArray()) {
			output.print(c);
		}
		output.flush();
	}

}
