package seiot.model.agent;

import java.io.IOException;

import seiot.log.LoggerFactory;
import seiot.model.core.AbstractState;
import seiot.model.core.Event;
import seiot.model.core.ReactiveAgent;
import seiot.model.device.Light;
import seiot.model.event.TimerDelta;
import seiot.model.msg.Msg;
import seiot.model.msg.MsgEvent;
import seiot.model.msg.MsgExit;
import seiot.model.msg.MsgFailedAccess;
import seiot.model.msg.MsgInside;
import seiot.model.msg.MsgLogin;
import seiot.model.msg.MsgOkAccess;
import seiot.model.msg.MsgTimeout;
/**
 *	A class with a state machine 
 */

public class Gateway extends ReactiveAgent{
	private static final String FAILED_ACCESS = "wrong credentials, access failed";
	private static final String FAILED_ENTER = "timeout exceeded, no person detected";
	private static final String START_SESSION = "person in the home..";
	private static final TimerDelta LIGHT = new TimerDelta(100);
	private Event currentEvent;
	private seiot.model.core.State currentState;
	private final Blinker failedAccess;
	private final Light inside;
	
	public Gateway(final Blinker failedAcces, final Light inside) {
		this.currentState = new Idle();
		this.failedAccess = failedAcces;
		this.inside = inside;
	}
	
	@Override
	protected void processEvent(Event ev) {
		this.currentEvent = ev;
		this.currentState.exec();
		this.currentState = this.currentState.nextState();
	}
	private abstract class MsgState extends AbstractState{

		@Override
		public void exec() {
			if(currentEvent instanceof MsgEvent) {
				produceResult((MsgEvent)currentEvent);
			}
		}
		
		public abstract void produceResult(final MsgEvent msg);
		
	}
	private class Idle extends MsgState {
		@Override
		public void produceResult(final MsgEvent event) {
			final Msg msg = event.getMsg();
			if(msg instanceof MsgLogin) {
				final MsgLogin login = (MsgLogin)msg;
				final ReactiveAgent sender = event.getFrom();
				if(LoginManager.checkCredentials(login.getUsername(), login.getPassword())) {
					//OK MSG
					sendMsgTo(sender, new MsgOkAccess());
					this._nextState = new WaitEntry();
				} else {
					//KO MSG
					try {
						LoggerFactory.instance().log(FAILED_ACCESS);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					failedAccess.notifyEvent(LIGHT);
					sendMsgTo(sender,new MsgFailedAccess());
				}
			}
		}
		
	}
	
	private class WaitEntry extends MsgState {
		@Override
		public void produceResult(MsgEvent event) {
			final Msg msg = event.getMsg();
			if(msg instanceof MsgInside) {
				//PERSON ENTER
				try {
					inside.switchOn();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					LoggerFactory.instance().log(START_SESSION);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				_nextState = new Indoor();
				
			} else if(msg instanceof MsgTimeout) {
				//FAILED ACCESS
				try {
					LoggerFactory.instance().log(FAILED_ENTER);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				failedAccess.notifyEvent(LIGHT);
				_nextState = new Idle();
			}
		}
	}
	
	private class Indoor extends MsgState {
		@Override
		public void produceResult(MsgEvent event) {
			final Msg msg = event.getMsg();
			if(msg instanceof MsgExit) {
				_nextState = new Idle();
				try {
					inside.switchOff();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
