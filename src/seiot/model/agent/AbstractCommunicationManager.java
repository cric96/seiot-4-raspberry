package seiot.model.agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import seiot.model.core.Event;
import seiot.model.core.ReactiveAgent;
import seiot.model.msg.Msg;
import seiot.model.msg.MsgEvent;
import seiot.model.msg.MsgExit;
import seiot.model.msg.MsgInside;
import seiot.model.msg.MsgLogin;
import seiot.model.msg.MsgTemp;
import seiot.model.msg.MsgTimeout;
import seiot.model.msg.MsgReiceved;
import seiot.model.msg.MsgToSend;
import seiot.model.msg.MsgValue;
/**
 * a root of communication manager, allow the communication
 * in output and input using a protocol
 */
public abstract class AbstractCommunicationManager extends ReactiveAgent{
	/*It indicates where to find the message type in a message: in the 0 position*/
	private static final int MESSAGE_TYPE = 0;
	private final List<ReactiveAgent> agents = new ArrayList<>();
	private final Map<String,MsgReiceved.MsgBuilder> stringToMsg = new HashMap<>();
	
	public AbstractCommunicationManager() {

		this.initializeMap();
	}
	
	public void attach(final ReactiveAgent agent) {
		this.agents.add(agent);
	}
	/*It process msg received from the others agents to "serial" */
	protected void processEvent(Event ev) {
		if(ev instanceof MsgEvent) {
			Msg msg = ((MsgEvent) ev).getMsg();
			if(msg instanceof MsgToSend) {
				String send = ((MsgToSend) msg).toProtocolForm();
				out(send);
			}
		}
	}
	/*this map links message type with Msg*/
	private final void initializeMap() {
		stringToMsg.put("L", new MsgLogin.MsgLoginBuilder());
		stringToMsg.put("E", new MsgTimeout.MsgTimeoutBuilder());
		stringToMsg.put("S", new MsgInside.MsgInsideBuilder());
		stringToMsg.put("F", new MsgExit.MsgExitBuilder());
		stringToMsg.put("T", new MsgTemp.MsgTempBuilder());
		stringToMsg.put("V", new MsgValue.MsgValueBuilder());
	}
	/**
	 * a method used to print the string into the outstream
	 */
	protected abstract void out(final String string);
	/**
	 * a method used to show the msg received from an input
	 * @param line
	 */
	protected final void sendTo(final String line) {
		if(!line.isEmpty()) {
			final String protocol = ""+line.charAt(MESSAGE_TYPE);
			if(stringToMsg.containsKey(protocol)) {
				final Optional<Msg> msg = stringToMsg.get(protocol).createMsg(line);
				if(msg.isPresent()) {
					for(final ReactiveAgent agent : this.agents) {
						sendMsgTo(agent, msg.get());
					}
				}
			}
		}
		
	}
}
