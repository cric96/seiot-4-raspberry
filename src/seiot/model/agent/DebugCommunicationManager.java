package seiot.model.agent;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class DebugCommunicationManager extends AbstractCommunicationManager{
	final JTextField field = new JTextField();
	public DebugCommunicationManager() {
		final JFrame window = new JFrame();
		final JPanel mainPane = new JPanel(new GridLayout(2,1));
		final JTextArea area = new JTextArea();
		final JButton insert = new JButton("Send msg..");
		insert.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sendTo(area.getText());
			}
		});
		final JPanel sendPane = new JPanel(new GridLayout(1,2));
		sendPane.add(area);
		sendPane.add(insert);
		field.setEditable(false);
		mainPane.add(field,0,0);
		mainPane.add(sendPane,1,0);
		window.getContentPane().add(mainPane);
		window.setTitle("ComunicationManager");
		window.setSize(new Dimension(400,400));
		window.setVisible(true);
		window.setDefaultCloseOperation(window.EXIT_ON_CLOSE);
	}
	
	@Override
	protected void out(String string) {
		field.setText(string);
	}

	
}
