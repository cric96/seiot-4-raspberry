package seiot.model.agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
/**
 * an utility class used to check login
 */
public class LoginManager {
	private LoginManager() {}
	private static final Map<String,String> CREDENTIAL = new HashMap<>();
	private static final String SEPARATOR = ";";
	private static final String FILE = "/credentials";
	static {
		final BufferedReader buff = new BufferedReader(new InputStreamReader(LoginManager.class.getResourceAsStream(FILE)));
		String line = null;
		try {
			while((line = buff.readLine())!= null) {
				final String[] credentials = line.split(SEPARATOR);
				CREDENTIAL.put(credentials[0], credentials[1]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * @param username the username to check
	 * @param password the password associated to the username
	 * @return true if the username and password is correct, false otherwise
	 */
	public static boolean checkCredentials(final String username, final String password) {
		if(CREDENTIAL.containsKey(username)) {
			if(CREDENTIAL.get(username).equals(password)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
}
