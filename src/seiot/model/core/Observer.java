package seiot.model.core;
/**
 * an observer of event
 */
public interface Observer {
	/**
	 * called by an observable to notify an event
	 * @param ev
	 * 	a generic event
	 * @return
	 * 	true if all is ok, false otherwhise
	 */
	boolean notifyEvent(Event ev);
}
