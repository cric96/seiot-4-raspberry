package seiot.model.msg;

/**
 * a root interface of msg to send
 */
public interface MsgToSend extends Msg{
	/**
	 * @return the protocol form of msg
	 */
	String toProtocolForm();
}
