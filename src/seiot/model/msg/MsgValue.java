package seiot.model.msg;

import java.util.Optional;
/**
 * a msg used to reiceve a l value
 */
public class MsgValue extends MsgReiceved{
	@Override
	public String toString() {
		return "MsgValue [value=" + value + "]";
	}

	private final int value;

	private MsgValue(int value) {
		super();
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public static class MsgValueBuilder extends MsgReiceved.MsgBuilder {
		@Override
		public Optional<Msg> createMsg(final String stringMsg) {
			final String[] words = stringMsg.split(separator);
			if(words.length != 2 && !words[0].equals("V")) {
				return Optional.empty();
			}
			try {
				System.out.println(words[1]);
				Integer.parseInt(words[1]);
			} catch(Exception e){
				return Optional.empty();
			}
			return Optional.of(new MsgValue(Integer.parseInt(words[1])));
		}
	}
}

