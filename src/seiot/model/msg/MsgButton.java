package seiot.model.msg;
/**
 * all message produce by a button
 */
public interface MsgButton {
    static class Off implements Msg { }
    static class On implements Msg { }
}
