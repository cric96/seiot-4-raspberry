package seiot.model.msg;

import java.util.Optional;

/**
 * a msg sent by a generic source
 * tells that the human is in the smart home
 */
public class MsgInside extends MsgReiceved{
	private MsgInside() {}
	public static class MsgInsideBuilder extends MsgReiceved.MsgBuilder {
		@Override
		public Optional<Msg> createMsg(final String stringMsg) {
			final String[] words = stringMsg.split(separator);
			if(words.length != 1 && !words[0].equals("S")) {
				return Optional.empty();
			}
			return Optional.of(new MsgInside());
		}
	}
}
