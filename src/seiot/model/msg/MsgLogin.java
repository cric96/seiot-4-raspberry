package seiot.model.msg;

import java.util.Optional;

/**
 * define a message with a username and password
 * msg reiceve to a source
 */
public class MsgLogin extends MsgReiceved {
	private final String username;
	private final String password;
	
	
	private MsgLogin(final String username, final String password) {
		super();
		this.username = username;
		this.password = password;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}
	/**
	 * @return a password associated to the username
	 */
	public String getPassword() {
		return this.password;
	}
	
	public static class MsgLoginBuilder extends MsgReiceved.MsgBuilder {
		@Override
		public Optional<Msg> createMsg(final String stringMsg) {
			final String[] words = stringMsg.split(separator);
			if(words.length != 3 && !words[0].equals("L")) {
				return Optional.empty();
			}
			return Optional.of(new MsgLogin(words[1], words[2]));
		}
	}
}
