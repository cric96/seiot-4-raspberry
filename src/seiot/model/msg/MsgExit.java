package seiot.model.msg;

import java.util.Optional;

/**
 * an msg send by a generic source
 * tells that the human exit to the smart system
 */
public class MsgExit extends MsgReiceved{
	private MsgExit() {}
	public static class MsgExitBuilder extends MsgReiceved.MsgBuilder {
		@Override
		public Optional<Msg> createMsg(final String stringMsg) {
			final String[] words = stringMsg.split(separator);
			if(words.length != 1 && !words[0].equals("F")) {
				return Optional.empty();
			}
			return Optional.of(new MsgExit());
		}
	}
}
