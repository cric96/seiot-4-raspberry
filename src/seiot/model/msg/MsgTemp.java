package seiot.model.msg;

import java.util.Optional;
/**
 * a msg used to tells the temp in the home
 */
public class MsgTemp extends MsgReiceved{
	@Override
	public String toString() {
		return "MsgTemp [temp=" + temp + "]";
	}
	private final Double temp;
	
	private MsgTemp(final Double temp) {
		this.temp = temp;
	}
	public Double getTemp() {
		return this.temp;
	}
	public static class MsgTempBuilder extends MsgReiceved.MsgBuilder {
		@Override
		public Optional<Msg> createMsg(final String stringMsg) {
			final String[] words = stringMsg.split(separator);
			if(words.length != 2 && !words[0].equals("T")) {
				return Optional.empty();
			}
			try {
				Double.parseDouble(words[1]);
			} catch(Exception e){
				return Optional.empty();
			}
			return Optional.of(new MsgTemp(Double.valueOf(words[1])));
		}
	}
}
