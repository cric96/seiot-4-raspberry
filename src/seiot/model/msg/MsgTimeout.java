package seiot.model.msg;

import java.util.Optional;

/**
 * a msg sent by a generic source
 * tells that the human don't enter in the home after a delta time
 */
public class MsgTimeout extends MsgReiceved {
	private MsgTimeout() {}
	public static class MsgTimeoutBuilder extends MsgReiceved.MsgBuilder {
		@Override
		public Optional<Msg> createMsg(final String stringMsg) {
			final String[] words = stringMsg.split(separator);
			if(words.length != 1 && !words[0].equals("E")) {
				return Optional.empty();
			}
			return Optional.of(new MsgTimeout());
		}
	}
}
