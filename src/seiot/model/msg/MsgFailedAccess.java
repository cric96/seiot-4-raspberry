package seiot.model.msg;

public class MsgFailedAccess implements MsgToSend {
	@Override
	public String toProtocolForm() {
		return "F\n";
	}
}
