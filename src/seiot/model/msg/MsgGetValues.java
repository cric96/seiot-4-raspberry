package seiot.model.msg;
/**
 * a msg used to get values in the home
 */
public class MsgGetValues implements MsgToSend{
	@Override
	public String toProtocolForm() {
		return "R\n";
	}

}
