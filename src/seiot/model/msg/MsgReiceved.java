package seiot.model.msg;

import java.util.Optional;
/**
 * a generic msg reiceve to a source
 */
public abstract class MsgReiceved implements Msg {
	/*It creates a Msg from a string, only if the string matches some known pattern*/
	public static abstract class MsgBuilder {
		protected final String separator = ";";
		public abstract Optional<Msg> createMsg(final String stringMsg);
	}
}
