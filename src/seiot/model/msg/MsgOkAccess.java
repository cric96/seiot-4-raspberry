package seiot.model.msg;
/**
 * a msg sent when the credentials are ok
 */
public class MsgOkAccess implements MsgToSend {
	@Override
	public String toProtocolForm() {
		return "A\n";
	}
}
